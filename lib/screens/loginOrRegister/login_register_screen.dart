import 'package:chat/components/primary_button.dart';
import 'package:chat/constants.dart';

import 'package:flutter/material.dart';

import 'package:chat/screens/loginOrRegister/register.dart';
import 'package:chat/screens/loginOrRegister/login.dart';

// ignore: unused_import

class Login_Page_Screen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: kDefaultPadding),
          child: Column(
            children: [
              Spacer(flex: 2),
              Image.asset(
                MediaQuery.of(context).platformBrightness == Brightness.light
                    ? "assets/images/Logo_home.png"
                    : "assets/images/Logo_home.png",
                height: 146,
              ),
              Spacer(),
              SizedBox(height: kDefaultPadding * 1.5),
              PrimaryButton(
                color: Theme.of(context).colorScheme.secondary,
                text: "Login",
                press: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Login_Screen(),
                  ),
                ),
              ),
              SizedBox(height: kDefaultPadding * 1.5),
              PrimaryButton(
                color: Theme.of(context).colorScheme.secondary,
                text: "Register",
                press: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Register_Screen(),
                  ),
                ),
              ),
              Spacer(flex: 2),
            ],
          ),
        ),
      ),
    );
  }
}
